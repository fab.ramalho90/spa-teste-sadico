# SPA-teste-sadico

## Instalação, Inicialização dos testes/aplicação e Acesso a aplicação
  ## Instalar dependências
  ```
  npm install
  ```
  ### Inicializar os testes
  ```
  npm run test:unit
  ```
  ### Inicializar a aplicação
  ```
  npm run serve
  ```
  ### Acessar aplicação
  ```
  http://localhost:8080
  ```

## Detalhamento da Solução

  ### Primeira Rota (Home.vue) - SPA com Humor Sério
  A SPA inicia na Primeira Rota (Home.vue) da aplicação que exibe uma frase relacionada ao humor da SPA, contexto,
  instruções ao usuário e o emoji atual (SPA com Humor Sério) da SPA com o link para a segunda Rota (SPA com Humor 100% Triste).
    Ao clicar no emoji, a aplicação irá direcionar para a segunda URL/Rota.

  ### Segunda Rota (Triste.vue) - SPA com Humor 100% Triste
  Na segunda Rota da SPA (Triste.vue), a aplicação irá exibir uma frase relacionada ao humor da SPA, contexto,
  instruções ao usuário e o emoji atual (SPA com Humor 100% Triste) da SPA com o link para a terceira Rota (SPA com Piada e Humor Progressivo).
  Ao clicar no emoji, a aplicação irá direcionar para a Terceira URL/Rota.

  ### Terceira Rota (Piada.vue) - SPA com Piada e Humor Progressivo
  Na Terceira Rota (Piada.vue), a aplicação irá exibir uma piada geek (aleatória) e irá realizar uma mudança de progressiva do humor do SPA através de emojis (100% Triste para 100% Feliz). Serão exibidos 7 emojis (1 emoji a cada 1 segundo). Ao exibir o emoji 100% Feliz, a aplicação irá exibir uma mensagem de agradecimento e irá exibir o botão Fechar para que o usuário retorne à Primeira Rota (Home.vue - SPA com Humor Sério).
    Ao clicar no botão Fechar, a aplicação irá direcionar para a primeira URL/Rota.

## Sr. SEO - Otimização para mecanismos de busca - Otimizações utilizadas

  * Tag Title - Index.html
  * Meta Tag Description - Index.html
  * Meta Tag Robots - Index.html
  * Tag h1 - Home.vue
  * Tag h1 - Triste.vue

## Tecnologias utilizadas

  * Node.js - JavaScript para fazer a manipulação dos arquivos e a composição da estrutura do projeto. Em conjunto com o NPM (Gerenciador de pacotes) para o gerenciamento de pacotes e builds.
  * Vue (VueCli) para construir os componentes em conjunto com frameworks/dependências Bootstrap Vue, Vuetify e Fontawesome
  * Jest para escrever os testes unitários
  * Vue para codificar o gerenciamento de estado (foi utilizado o gerenciador de estados nativo do Vue)
  * Biblioteca Axios para a utilização de requisições http (API)

## Estrutura de pastas do projeto

  * public
    * img/humor - Emojis do humor do SPA
    * index.html - Página padrão dentro dos diretórios nos servidores de websites e onde são realizadas algumas das boas práticas do Sr. SEO
  * src
    * api - Acesso as api de piada
    * assets - Configuração das bibliotecas de CSS (Bootstrap e Vuetify)
    * components - Componentes globais da aplicação
    * plugins - Configurações de plugins, como, axios e vuetify
    * views - Páginas das rotas da aplicação
    * App.vue - Componente base da aplicação
    * main.ts - Inicializador do vue
    * router.ts - Configuração das rotaas da aplicação
  * tests
    * unit - Testes unitários da aplicação
